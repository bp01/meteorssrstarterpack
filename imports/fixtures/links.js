
import Links from '../api/links/links';

function insertLink(title, url) {
    Links.insert({ title, url, createdAt: new Date() });
}

// If the Links collection is empty, add some data.
function createLinks() {
    if (Links.find().count() === 0) {
        insertLink(
            'Do the Tutorial',
            'https://www.meteor.com/tutorials/react/creating-an-app'
        );

        insertLink(
            'Follow the Guide',
            'http://guide.meteor.com'
        );

        insertLink(
            'Read the Docs',
            'https://docs.meteor.com'
        );

        insertLink(
            'Discussions',
            'https://forums.meteor.com'
        );
    }
}


export {createLinks}