import { Meteor } from 'meteor/meteor';
import {createLinks} from './links';

const runFixtures = function () {
    // const shouldRun = Meteor.users.find().count() == 0;
    const shouldRun = true;
    
    if (shouldRun) {
        createLinks();
    }
}
if (Meteor.isDevelopment) {
    runFixtures();
}