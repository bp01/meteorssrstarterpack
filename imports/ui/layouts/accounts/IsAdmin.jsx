/* eslint-disable react/prop-types, react/jsx-no-bind */
import React from 'react';
import { Route } from 'react-router-dom';
import AccessDenied from '../../pages/errors/accessDenied/AccessDenied';

const IsAdmin = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      if (rest && rest.profile && rest.profile.isAdmin) {
        return <Component {...rest} {...props} />;
      }

      return <AccessDenied />;
    }}
  />
);

export default IsAdmin;
