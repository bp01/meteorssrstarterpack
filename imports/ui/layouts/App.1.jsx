/* eslint-disable react/require-default-props, react/forbid-prop-types */
// eslint-disable-next-line no-unused-vars
import $ from 'jquery';
// eslint-disable-next-line no-unused-vars
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope, faLock } from '@fortawesome/free-solid-svg-icons';

import Profiles from '../../api/profiles/profiles';
// import Info from '../ui/pages/Info.jsx';
import Navbar from '../components/navs/Navbar';

// Public pages
import Signup from '../pages/accounts/signup/Signup';
import Login from '../pages/accounts/login/Login';
import ForgotPassword from '../pages/accounts/forgotPassword/ForgotPassword';
import ResetPassword from '../pages/accounts/resetPassword/ResetPassword';
import EnrollAccount from '../pages/accounts/enrollAccount/EnrollAccount';
import Terms from '../pages/public/legal/Terms';
import Privacy from '../pages/public/legal/Privacy';
import Error404 from '../pages/errors/404/Errors404';

// Admin Pages
import CreateUsers from '../pages/accounts/createUsers/CreateUsers';

// Route access
import IsAdmin from './accounts/IsAdmin';

// font awesome library
library.add(faEnvelope, faLock);

const Index = () => <h1>Home page. index test.</h1>;
const Loading = () => <p>Loading...</p>;

const App = props => {
  const { loading } = props;
  const { user } = props;
  const { loadingProfileHandle } = props;
  const { profile } = props;

  if (loading && !loadingProfileHandle) {
    return (
      <Router>
        <div className="application">
          <Helmet>
            <meta charSet="utf-8" />
            <title>Full Stack Recruitment</title>
            <link rel="canonical" href="https://thefsrapp.com" />
          </Helmet>
          <Navbar user={user} profile={profile} />
          <Switch>
            <Route path="/" exact component={Index} />
            <Route path="/login" exact component={Login} />
            <Route path="/signup" exact component={Signup} />
            <Route path="/users/password/new" exact component={ForgotPassword} />
            <Route path="/users/password/reset/:token" exact component={ResetPassword} />
            <Route path="/users/password/enrollment/:token" exact component={EnrollAccount} />
            <Route path="/terms" exact component={Terms} />
            <Route path="/privacy" exact component={Privacy} />

            {/* Admin */}
            <IsAdmin path="/admin/users/create" exact component={CreateUsers} {...props} />

            {/* Page not found */}
            <Route exact component={Error404} />
          </Switch>
        </div>
      </Router>
    );
  }

  return <Loading />;
};

const AppContainer = withTracker(() => {
  const loading = Accounts.loginServicesConfigured();

  const profileHandle = Meteor.subscribe('publish-user-profile');
  const loadingProfileHandle = !profileHandle.ready();
  const profile = Profiles.findOne({ userId: Meteor.userId() });

  return {
    loading,
    user: Meteor.user(),
    loadingProfileHandle,
    profile
  };
})(App);

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  loadingProfileHandle: PropTypes.bool.isRequired,
  user: PropTypes.object,
  profile: PropTypes.object
};

export default AppContainer;
