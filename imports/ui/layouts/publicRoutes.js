import React from 'react';

import { Route } from 'react-router-dom';

const Index = () => <h1>Home page. index test.</h1>;

const publicRoutes = <Route path="/" exact component={Index} />;

export default publicRoutes;
