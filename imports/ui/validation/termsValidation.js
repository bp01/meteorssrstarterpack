import isEmpty from 'validator/lib/isEmpty';

function termsValidation(terms) {
  let termsErrors;

  if (!terms) {
    termsErrors = 'You must agree to the Terms of Service and Privacy Policy to proceed';
  }

  return termsErrors;
}

export default termsValidation;
