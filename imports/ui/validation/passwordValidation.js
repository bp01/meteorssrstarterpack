import isEmpty from 'validator/lib/isEmpty';

function passwordValidation(password) {
  let passwordErrors;

  if (isEmpty(password) || password.length < 8) {
    passwordErrors = 'Password is required and needs to be at least 8 characters';
  }

  return passwordErrors;
}

export default passwordValidation;
