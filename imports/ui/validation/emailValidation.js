import isEmpty from 'validator/lib/isEmpty';
import isEmail from 'validator/lib/isEmail';

function emailValidation(email) {
  let emailErrors;

  if (isEmpty(email)) {
    emailErrors = 'Email is required';
  }

  if (!isEmail(email)) {
    emailErrors = 'Email is invalid';
  }

  return emailErrors;
}

export default emailValidation;
