/* eslint-disable react/require-default-props, react/forbid-prop-types */
import { Meteor } from 'meteor/meteor';
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

export default class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.handleLogout = this.handleLogout.bind(this);
  }

  handleLogout = e => {
    e.preventDefault();
    Meteor.logout();
  };

  render() {
    const { user } = this.props;
    const { profile } = this.props;

    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <NavLink className="navbar-brand" to="/">
          Logo
        </NavLink>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ml-auto">
            {profile && profile.isAdmin ? (
              <li className="nav-item">
                <NavLink className="nav-link" exact to="/admin/users/create">
                  Create user
                </NavLink>
              </li>
            ) : null}

            <li className="nav-item">
              <NavLink className="nav-link" exact to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" exact to="/about">
                About
              </NavLink>
            </li>
            {!user ? (
              <React.Fragment>
                <li className="nav-item">
                  <NavLink className="nav-link" exact to="/signup">
                    Sign up
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" exact to="/login">
                    Login
                  </NavLink>
                </li>
              </React.Fragment>
            ) : (
              <li className="nav-item">
                <a href="_blank" className="nav-link" onClick={this.handleLogout}>
                  Logout
                </a>
              </li>
            )}
          </ul>
        </div>
      </nav>
    );
  }
}

Navbar.propTypes = {
  user: PropTypes.object,
  profile: PropTypes.object
};
