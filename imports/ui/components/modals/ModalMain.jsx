/* eslint-disable react/require-default-props */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ModalMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  render() {
    const { modal } = this.state;
    const { className } = this.props;
    const { ModalHeaderContent } = this.props;
    const { ModalBodyContent } = this.props;
    const { ModalHeaderExist } = this.props;
    return (
      <div>
        <Modal isOpen={modal} toggle={this.toggle} className={className} centered>
          {ModalHeaderExist ? (
            <ModalHeader toggle={this.toggle}>{ModalHeaderContent}</ModalHeader>
          ) : null}
          <ModalBody>{ModalBodyContent}</ModalBody>
          <ModalFooter>
            <Button color="secondary" block onClick={this.toggle}>
              OK
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

ModalMain.propTypes = {
  className: PropTypes.string,
  ModalHeaderContent: PropTypes.string,
  ModalHeaderExist: PropTypes.bool.isRequired,
  ModalBodyContent: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired
};

export default ModalMain;
