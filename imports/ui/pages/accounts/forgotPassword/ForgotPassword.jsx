/* eslint-disable react/no-this-in-sfc, react/jsx-no-bind */

import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';
import ModalMain from '../../../components/modals/ModalMain';

import forgotPasswordValidation from './forgotPasswordValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function ForgotPassword() {
  const [email, setEmail] = useState('');

  const [formErrors, setFormErrors] = useState({});

  function handleEmailChange(e) {
    setEmail(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = forgotPasswordValidation(email);

    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    if (isObjectEmpty(errorsInForm)) {
      Accounts.forgotPassword({ email }, err => {
        if (err) {
          setFormErrors({ server: `Hmmmm. Something's gone wrong.` });
        }
        if (!err) {
          this.child.toggle();
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Forgot password - ${Meteor.settings.public.companyName}`}
        description="Forgot your password? Not to worry! You can reset your password right here."
      />

      <ModalMain
        ref={instance => {
          this.child = instance;
        }}
        ModalHeaderExist={false}
        ModalBodyContent="You will receive a password update link in your email shortly.  Please check your spam folder."
      />

      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="forgot-password-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center mb-4">Forgot password</h2>
              {!formErrors.server ? null : (
                <div className="alert alert-danger text-center" role="alert">
                  {formErrors && formErrors.server}
                </div>
              )}
              <p>
                Enter your email address below and we’ll send an email with a link to update your
                password.
              </p>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="envelope" />
                    </div>
                  </div>
                  <input
                    type="email"
                    className={!formErrors.email ? 'form-control' : 'form-control is-invalid'}
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={handleEmailChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.email}</div>
                </div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Send
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
