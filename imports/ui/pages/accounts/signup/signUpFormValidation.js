import emailValidation from '../../../validation/emailValidation';
import passwordValidation from '../../../validation/passwordValidation';
import termsValidation from '../../../validation/termsValidation';

function signUpFormValidation(email, password, terms) {
  const errors = {};

  // check for signup form errors
  const emailErrors = emailValidation(email);
  const passwordErrors = passwordValidation(password);
  const termsErrors = termsValidation(terms);

  // if there are errors return error object to display errors on singup form
  if (emailErrors) {
    errors.email = emailErrors;
  }

  if (passwordErrors) {
    errors.password = passwordErrors;
  }

  if (termsErrors) {
    errors.terms = termsErrors;
  }

  return errors;
}

export default signUpFormValidation;
