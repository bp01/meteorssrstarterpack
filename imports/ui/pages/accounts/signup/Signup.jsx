/* eslint-disable no-alert */
import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';

import signUpFormValidation from './signUpFormValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function Signup() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [terms, setTerms] = useState(false);

  const [formErrors, setFormErrors] = useState({});

  function handleEmailChange(e) {
    setEmail(e.target.value);
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleTermsChange(e) {
    setTerms(e.target.checked);
  }

  function handleLogin() {
    Meteor.loginWithPassword(email, password, err => {
      if (err) {
        alert('hmmmmm, something went wrong. Please try and login');
      } else {
        alert('yes you have logged in');
      }
    });
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = signUpFormValidation(email, password, terms);

    // display sign up errors on form
    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    // create new account. If error returns on server. Error is displayed on form.
    if (isObjectEmpty(errorsInForm)) {
      Meteor.call('user.register', email, password, terms, err => {
        if (err) {
          formErrors.email = err.reason;
          setFormErrors(formErrors);
        } else {
          handleLogin();
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Sign up - Create Your ${Meteor.settings.public.companyName} Account`}
        description={`Sign up for a ${
          Meteor.settings.public.companyName
        } account & Get full stack and front end developer job offers`}
      />
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="sign-up-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center">Sign up</h2>
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="envelope" />
                    </div>
                  </div>
                  <input
                    type="email"
                    className={!formErrors.email ? 'form-control' : 'form-control is-invalid'}
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={handleEmailChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.email}</div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="lock" />
                    </div>
                  </div>
                  <input
                    type="password"
                    className={!formErrors.password ? 'form-control' : 'form-control is-invalid'}
                    id="password"
                    aria-describedby="passwordHelp"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handlePasswordChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.password}</div>
                </div>
              </div>

              <div className="form-group custom-control custom-checkbox">
                <input
                  type="checkbox"
                  className={
                    !formErrors.terms ? 'custom-control-input' : 'custom-control-input is-invalid'
                  }
                  id="terms"
                  name="terms"
                  checked={terms}
                  onChange={handleTermsChange}
                />
                <label className="custom-control-label small" htmlFor="terms">
                  By signing up you agree to {Meteor.settings.public.companyName}{' '}
                  <Link to="/terms">Terms of Service</Link> and{' '}
                  <Link to="/privacy">Privacy Policy</Link>.
                </label>
                <div className="invalid-feedback">{formErrors && formErrors.terms}</div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Sign up
              </button>
            </form>
          </div>
          <p className="small text-center mt-4">
            Already have an account? <Link to="login">Login.</Link>
          </p>
        </div>
      </div>
    </div>
  );
}
