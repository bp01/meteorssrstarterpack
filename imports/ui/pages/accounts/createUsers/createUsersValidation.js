import emailValidation from '../../../validation/emailValidation';

function createUsersValidation(email) {
  const errors = {};

  // check for signup form errors
  const emailErrors = emailValidation(email);

  // if there are errors return error object to display errors on singup form
  if (emailErrors) {
    errors.email = emailErrors;
  }

  return errors;
}

export default createUsersValidation;
