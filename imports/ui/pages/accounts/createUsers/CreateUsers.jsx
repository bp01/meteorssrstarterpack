/* eslint-disable react/jsx-no-bind, react/no-this-in-sfc, no-alert */

import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';
import ModalMain from '../../../components/modals/ModalMain';

import createUsersValidation from './createUsersValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function CreateUsers() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');

  const [formErrors, setFormErrors] = useState({});

  function handleFirstNameChange(e) {
    setFirstName(e.target.value);
  }

  function handleLastNameChange(e) {
    setLastName(e.target.value);
  }

  function handleEmailChange(e) {
    setEmail(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = createUsersValidation(email);

    // display sign up errors on form
    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    // // create new account. If error returns on server. Error is displayed on form.
    if (isObjectEmpty(errorsInForm)) {
      Meteor.call('admin.create.users', email, firstName, lastName, err => {
        if (err) {
          setFormErrors({ server: err.reason });
        } else {
          this.child.toggle();
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Create new ${Meteor.settings.public.companyName} Account`}
        description={`Create a new ${
          Meteor.settings.public.companyName
        } account & Get full stack and front end developer job offers`}
      />

      <ModalMain
        ref={instance => {
          this.child = instance;
        }}
        ModalHeaderExist={false}
        ModalBodyContent={
          <React.Fragment>
            New account created. Enrolment email sent.
            <br />
            Name: {firstName} {lastName}
            <br /> email: {email}
          </React.Fragment>
        }
      />
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="create-users-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center mb-4">Create users</h2>
              {!formErrors.server ? null : (
                <div className="alert alert-danger text-center" role="alert">
                  {formErrors && formErrors.server}
                </div>
              )}
              <div className="form-group">
                <div className="row">
                  <div className="col">
                    <label htmlFor="email">First name</label>
                    <input
                      type="text"
                      className={!formErrors.firstName ? 'form-control' : 'form-control is-invalid'}
                      aria-describedby="firstNameHelp"
                      placeholder="First name"
                      name="firstName"
                      value={firstName}
                      onChange={handleFirstNameChange}
                    />
                  </div>
                  <div className="col">
                    <label htmlFor="email">Last name</label>
                    <input
                      type="text"
                      className={!formErrors.lastName ? 'form-control' : 'form-control is-invalid'}
                      aria-describedby="lastNameHelp"
                      placeholder="Last name"
                      name="lastName"
                      value={lastName}
                      onChange={handleLastNameChange}
                    />
                  </div>
                </div>
              </div>

              <div className="form-group">
                <label htmlFor="email">Email</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="envelope" />
                    </div>
                  </div>
                  <input
                    type="email"
                    className={!formErrors.email ? 'form-control' : 'form-control is-invalid'}
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={handleEmailChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.email}</div>
                </div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Sign up
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
