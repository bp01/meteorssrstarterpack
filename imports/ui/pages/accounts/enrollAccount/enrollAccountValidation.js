import passwordValidation from '../../../validation/passwordValidation';
import termsValidation from '../../../validation/termsValidation';

function enrollAccountValidation(password, terms) {
  const errors = {};

  // check for reset password form errors
  const passwordErrors = passwordValidation(password);
  const termsErrors = termsValidation(terms);

  // if there are errors return error object to display errors on singup form
  if (passwordErrors) {
    errors.password = passwordErrors;
  }

  if (termsErrors) {
    errors.terms = termsErrors;
  }

  return errors;
}

export default enrollAccountValidation;
