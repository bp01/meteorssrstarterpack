/* eslint-disable react/no-this-in-sfc, react/jsx-no-bind */

import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';
import ModalMain from '../../../components/modals/ModalMain';

import enrollAccountValidation from './enrollAccountValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function ForgotPassword({ match }) {
  const [formErrors, setFormErrors] = useState({});
  const [password, setPassword] = useState('');
  const [terms, setTerms] = useState(false);

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleTermsChange(e) {
    setTerms(e.target.checked);
  }

  function handleAddProfileTerms() {
    Meteor.call('user.enroll.agree.terms', terms, err => {
      if (err) {
        setFormErrors({ server: err.reason });
      } else {
        this.child.toggle();
      }
    });
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = enrollAccountValidation(password, terms);

    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    if (isObjectEmpty(errorsInForm)) {
      const { token } = match.params;
      Accounts.resetPassword(token, password, err => {
        if (err) {
          setFormErrors({ server: err.reason });
        }
        if (!err) {
          handleAddProfileTerms();
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Set password - ${Meteor.settings.public.companyName}`}
        description="Set your password to access your account."
      />

      <ModalMain
        ref={instance => {
          this.child = instance;
        }}
        ModalHeaderExist={false}
        ModalBodyContent="You have created your password and you are now signed in."
      />

      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="reset-password-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center mb-4">Enroll account</h2>
              {!formErrors.server ? null : (
                <div className="alert alert-danger text-center" role="alert">
                  {formErrors && formErrors.server}
                </div>
              )}
              <p>Set your password to sign in to your account.</p>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="lock" />
                    </div>
                  </div>
                  <input
                    type="password"
                    className={!formErrors.password ? 'form-control' : 'form-control is-invalid'}
                    id="password"
                    aria-describedby="passwordHelp"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handlePasswordChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.password}</div>
                </div>
              </div>

              <div className="form-group custom-control custom-checkbox">
                <input
                  type="checkbox"
                  className={
                    !formErrors.terms ? 'custom-control-input' : 'custom-control-input is-invalid'
                  }
                  id="terms"
                  name="terms"
                  checked={terms}
                  onChange={handleTermsChange}
                />
                <label className="custom-control-label small" htmlFor="terms">
                  By signing up you agree to {Meteor.settings.public.companyName}{' '}
                  <Link to="/terms">Terms of Service</Link> and{' '}
                  <Link to="/privacy">Privacy Policy</Link>.
                </label>
                <div className="invalid-feedback">{formErrors && formErrors.terms}</div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Reset password
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
