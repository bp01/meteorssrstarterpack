import passwordValidation from '../../../validation/passwordValidation';

function forgotPasswordValidation(password) {
  const errors = {};

  // check for reset password form errors
  const passwordErrors = passwordValidation(password);

  // if there are errors return error object to display errors on singup form
  if (passwordErrors) {
    errors.password = passwordErrors;
  }

  return errors;
}

export default forgotPasswordValidation;
