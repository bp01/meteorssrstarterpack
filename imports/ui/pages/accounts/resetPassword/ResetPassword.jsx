/* eslint-disable react/no-this-in-sfc, react/jsx-no-bind */

import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';
import ModalMain from '../../../components/modals/ModalMain';

import resetPasswordValidation from './resetPasswordValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function ForgotPassword({ match }) {
  const [formErrors, setFormErrors] = useState({});
  const [password, setPassword] = useState('');

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = resetPasswordValidation(password);

    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    if (isObjectEmpty(errorsInForm)) {
      const { token } = match.params;
      Accounts.resetPassword(token, password, err => {
        if (err) {
          setFormErrors({ server: err.reason });
        }
        if (!err) {
          this.child.toggle();
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Reset password - ${Meteor.settings.public.companyName}`}
        description="Reset your password? Not to worry! You can reset your password right here."
      />

      <ModalMain
        ref={instance => {
          this.child = instance;
        }}
        ModalHeaderExist={false}
        ModalBodyContent="Your password has been reset and you are now signed in."
      />

      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="reset-password-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center mb-4">Reset password</h2>
              {!formErrors.server ? null : (
                <div className="alert alert-danger text-center" role="alert">
                  {formErrors && formErrors.server}
                </div>
              )}

              <div className="form-group">
                <label htmlFor="password">Password</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="lock" />
                    </div>
                  </div>
                  <input
                    type="password"
                    className={!formErrors.password ? 'form-control' : 'form-control is-invalid'}
                    id="password"
                    aria-describedby="passwordHelp"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handlePasswordChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.password}</div>
                </div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Reset password
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
