/* eslint-disable no-alert */
import { Meteor } from 'meteor/meteor';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import Header from '../../../components/header/Header';

import loginFormValidation from './loginFormValidation';
import isObjectEmpty from '../../../validation/isObjectEmpty';

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [formErrors, setFormErrors] = useState({});

  function handleEmailChange(e) {
    setEmail(e.target.value);
  }

  function handlePasswordChange(e) {
    setPassword(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();

    const errorsInForm = loginFormValidation(email, password);

    if (errorsInForm) {
      setFormErrors(errorsInForm);
    }

    if (isObjectEmpty(errorsInForm)) {
      Meteor.loginWithPassword(email, password, err => {
        if (err) {
          setFormErrors({ server: 'Invalid email or password.' });
        } else {
          alert('yes you have logged in');
        }
      });
    }
  }

  return (
    <div className="container h-100">
      <Header
        title={`Log in - ${Meteor.settings.public.companyName}`}
        description={`Log in to ${Meteor.settings.public.companyName} here. ${
          Meteor.settings.public.companyName
        } gets you full stack and front end developer job offers`}
      />

      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-7 col-lg-5">
          <div className="login-up-container">
            <form className="needs-validation" noValidate>
              <h2 className="text-center">Log in</h2>
              {!formErrors.server ? null : (
                <div className="alert alert-danger text-center" role="alert">
                  {formErrors && formErrors.server}
                </div>
              )}
              <div className="form-group">
                <label htmlFor="email">Email</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="envelope" />
                    </div>
                  </div>
                  <input
                    type="email"
                    className={!formErrors.email ? 'form-control' : 'form-control is-invalid'}
                    id="email"
                    aria-describedby="emailHelp"
                    placeholder="Email"
                    name="email"
                    value={email}
                    onChange={handleEmailChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.email}</div>
                </div>
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <div className="input-group mb-2">
                  <div className="input-group-prepend">
                    <div className="input-group-text">
                      <FontAwesomeIcon icon="lock" />
                    </div>
                  </div>
                  <input
                    type="password"
                    className={!formErrors.password ? 'form-control' : 'form-control is-invalid'}
                    id="password"
                    aria-describedby="passwordHelp"
                    placeholder="Password"
                    name="password"
                    value={password}
                    onChange={handlePasswordChange}
                  />
                  <div className="invalid-feedback">{formErrors && formErrors.password}</div>
                </div>
              </div>

              <button type="submit" className="btn btn-primary btn-block" onClick={handleSubmit}>
                Log in
              </button>
              <p className="small text-center mt-4 mb-0">
                <Link to="users/password/new">Forgot your password?</Link>
              </p>
            </form>
          </div>
          <p className="small text-center mt-4">
            Don&apos;t have an account? <Link to="signup">Sign up.</Link>
          </p>
        </div>
      </div>
    </div>
  );
}
