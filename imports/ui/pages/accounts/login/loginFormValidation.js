import emailValidation from '../../../validation/emailValidation';
import passwordValidation from '../../../validation/passwordValidation';

function loginFormValidation(email, password) {
  const errors = {};

  // check for signup form errors
  const emailErrors = emailValidation(email);
  const passwordErrors = passwordValidation(password);

  // if there are errors return error object to display errors on singup form
  if (emailErrors) {
    errors.email = emailErrors;
  }

  if (passwordErrors) {
    errors.password = passwordErrors;
  }

  return errors;
}

export default loginFormValidation;
