/* eslint-disable no-alert */
import React from 'react';

export default function AccessDenied() {
  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-8 col-lg-6">
          <div className="access-denied-container">
            <h2 className="text-center mb-3">ACCESS DENIED</h2>
            <p className="text-center mb-0">It looks like you might be in the wrong place.</p>
          </div>
        </div>
      </div>
    </div>
  );
}
