/* eslint-disable no-alert */
import React from 'react';

export default function Errors404() {
  return (
    <div className="container h-100">
      <div className="row h-100 justify-content-center align-items-center">
        <div className="col-md-8 col-lg-6">
          <div className="page-not-found-container">
            <h2 className="text-center mb-3">PAGE NOT FOUND</h2>
            <p className="text-center mb-0">
              The link you followed may be broken, or the page may have been removed.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
