import React from 'react';
import ReactDOM from 'react-dom';
import { onPageLoad } from 'meteor/server-render';
import { BrowserRouter } from 'react-router-dom';

onPageLoad(async sink => {
  const App = (await import('./client-routes.js')).default;

  ReactDOM.hydrate(
    <BrowserRouter>
      <App />
    </BrowserRouter>,
    document.getElementById('app')
  );
});

// onPageLoad(async sink => {
//   const App = (await import('./client-routes.js')).default;

//   ReactDOM.hydrate(<App />, document.getElementById('app'));
// });
