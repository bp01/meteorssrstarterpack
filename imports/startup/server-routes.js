import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Login from '../ui/pages/accounts/login/Login';

export default function myServerRoutes() {
  return (
    <Switch>
      <Route path="/login" exact component={Login} />
    </Switch>
  );
}
