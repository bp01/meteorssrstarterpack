import React from 'react';
import { renderToString } from 'react-dom/server';
import { onPageLoad } from 'meteor/server-render';
import { StaticRouter } from 'react-router-dom';

import App from './server-routes';

const context = {};

onPageLoad(sink => {
  sink.renderIntoElementById(
    'app',
    renderToString(
      <StaticRouter location={sink.request.url} context={context}>
        <App />
      </StaticRouter>
    )
  );
});
