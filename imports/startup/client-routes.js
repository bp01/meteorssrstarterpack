import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Login from '../ui/pages/accounts/login/Login';

const ClientPage = () => <h1>Client index.</h1>;

export default function myClientRoutes() {
  return (
    <Switch>
      <Route path="/login" exact component={Login} />
      <Route path="/ClientPage" component={ClientPage} />
    </Switch>
  );
}
