/* eslint-disable func-names */
import { Meteor } from 'meteor/meteor';
import Profiles from '../profiles';

Meteor.publish('publish-user-profile', function() {
  if (this.userId) {
    return Profiles.find({ userId: Meteor.userId() });
  }
  return [];
});
