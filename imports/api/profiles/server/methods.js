/* eslint-disable func-names */
import { Meteor } from 'meteor/meteor';
import Profiles from '../profiles';

Meteor.methods({
  'user.enroll.agree.terms': function(terms) {
    Profiles.update({ userId: this.userId }, { $set: { terms } });
  }
});
