/* eslint-disable prettier/prettier, no-undef */

import { Mongo } from 'meteor/mongo';

export default Profiles = new Mongo.Collection('profiles');