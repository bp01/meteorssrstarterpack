/* eslint-disable prettier/prettier, no-undef */

import { Mongo } from 'meteor/mongo';

export default Links = new Mongo.Collection('links');
