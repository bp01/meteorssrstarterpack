import { Meteor } from 'meteor/meteor';
import Links from '../links';

Meteor.publish('compapublish-links', function () {
    return Links.find();
})