/* eslint-disable func-names */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
  'user.register': function(email, password, terms) {
    const isCandidate = true;
    Accounts.createUser({
      email,
      password,
      terms,
      isCandidate
    });
  },
  'admin.create.users': function(email, firstName, lastName) {
    // check if user already has an account
    const user = Accounts.findUserByEmail(email);

    // if user already exists return error to let admin know user has an account.
    if (user) {
      throw new Meteor.Error('user-email-exsits', 'User email already exists.');
    }

    if (!user) {
      // create user
      const isCandidate = true;
      const userId = Accounts.createUser({ email, firstName, lastName, isCandidate });
      if (userId) {
        Accounts.sendEnrollmentEmail(userId, email, { firstName, lastName });
      }
    }
  }
});
