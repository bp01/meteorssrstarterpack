/* eslint-disable func-names */
import { Accounts } from 'meteor/accounts-base';
import Profiles from '../../profiles/profiles';

Accounts.onCreateUser((options, user) => {
  if (options.isCandidate) {
    Profiles.insert({
      createdAt: new Date(),
      userId: user._id,
      terms: options.terms,
      firstName: options.firstName,
      lastName: options.lastName,
      isCandidate: options.isCandidate
    });
  }

  return user;
});
