/* eslint-disable func-names, no-undef, no-unused-vars */

import { Meteor } from 'meteor/meteor';
import assert from 'assert';

describe('createuser', function() {
  if (Meteor.isServer) {
    import './methods';

    const email = 'test@test.test';
    const password = '12345';

    it('create a new user', function() {
      Meteor.call('user.register', email, password, (err, result) => {
        assert.strictEqual(err.reason, 'Email already exists.');
      });
    });

    it('Email already exists', function() {
      Meteor.call('user.register', email, password, (err, result) => {
        assert.strictEqual(err.reason, 'Email already exists.');
      });
    });
  }
});
