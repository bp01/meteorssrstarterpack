/* eslint-disable prettier/prettier */
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

// Set password for new account created by admin
Accounts.emailTemplates.enrollAccount.from = () => Meteor.settings.public.applicationEmail_NoReply;
Accounts.emailTemplates.enrollAccount.text = (user, url) => {
  const newUrl = url.replace('#/enroll-account', 'users/password/enrollment');
  return `Hi ${user.services.password.reset.firstName} \nPlease use link below to set the password for your account. ${newUrl}`;
};
