import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';

// Reset password settings
Accounts.emailTemplates.resetPassword.from = () => Meteor.settings.public.applicationEmail_NoReply;
Accounts.emailTemplates.resetPassword.text = (user, url) => {
  const newUrl = url.replace('#/reset-password', 'users/password/reset');
  return `To reset your password, simply click the link below. ${newUrl}`;
};
