// Collections / documents
import './links/links';
import './links/server/publications';

import './users/server/methods';
import './users/server/onCreateUser';

import './profiles/profiles';
import './profiles/server/publications';
import './profiles/server/methods';

// emails
import './emails/index.js';
