module.exports = {
    "parser": "babel-eslint",
    "parserOptions": {
        "allowImportExportEverywhere": true,
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "env": {
        "es6":     true,
        "browser": true,
        "node":    true,
    },
    "plugins": [
        "meteor",
        "react",
        "prettier"
    ],
    "extends": [
        "airbnb",
        "plugin:meteor/recommended",
        "plugin:prettier/recommended",
        "prettier/react"
    ],
    "settings": {
        "import/resolver": "meteor",
    },
    "rules": {
        "react/jsx-filename-extension": [1, {
            "extensions": [".jsx"]
        }],
        "react/jsx-no-bind": [2, {
            "ignoreRefs": false,
            "allowArrowFunctions": false,
            "allowFunctions": false,
            "allowBind": false
        }],
        "jsx-a11y/label-has-associated-control": "off",
        "jsx-a11y/label-has-for":"off",
        "import/no-absolute-path": [0],
        "meteor/audit-argument-checks": [0],
        "switch-colon-spacing": [0],
        "no-invalid-this": [0],
        "new-cap": [1],
        "prettier/prettier": "error",
        "import/extensions": "off",
        "import/no-unresolved": "off"
    },
    "overrides": {
        files: "*.js,*.jsx",
    }
};